/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2015  gumbicp <email>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */
#ifndef _MAINMENU_H_
#define _MAINMENU_H_

#include "tmenu.hpp"
#include "wiremenu.hpp"

namespace rk {
const std::vector<std::string> MAINMENU_OPTIONS = {
          "\tPodręczny Kalkulator Elektronika.\n",
          "Opcje:\n",
          "\t1) Rezystancje Przewodów.\n",
          "\t2) Prawo Oma.\n",
          "\t0) Zakończ Program.\n"
     };

const short MAINMENU_SIZE = 3;
/**
 * Klasa dla głównego menu w terminalu.
 */
class MainMenu: public Menu {
	
public:
     MainMenu ();
     virtual ~MainMenu();
     void main_loop();


}; //class
}; // namespace
#endif /* _MAINMENU_H_*/
