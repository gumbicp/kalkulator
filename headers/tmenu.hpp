/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2015  gumbicp <email>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef _TMENU_H_
#define _TMENU_H_

#include <vector>
#include <boost/lexical_cast.hpp>


#include "debug.hpp"
#include "stale.hpp"
#include "funkcje.hpp"
#include "rez_kab.hpp"
#include "kolor.hpp"


namespace rk {

typedef void (*fprkcolor)();
/**
 * Klasa tworzy szablon menu w konsoli systemowej.
 * Na bazie tej klasy tworzymy całe menu programu
 * i kolejne pod menu dla danych opcji.
 */
class Menu {

public:
     /**
      * Konstruktor klasy Menu.
      * @param opts opcje menu.
      * @param s liczba opcji wyboru w menu.
      */
     Menu ( std::vector<std::string> opts, short s );
     virtual~Menu();

     /**
      * Metoda czysto wirtualna do implementacji
      * w klasach pochodnych.
      */
     virtual void main_loop() = 0;
     /**
       * Metoda ustawia kolor menu.
       * @param (void ( *pointer ) () ) adres do metody koloru .
       */
     void set_menu_color ( void ( *pointer ) () );

private:
     /*
      * Wskaźnik na funkcje koloru tekstu (wyjście na terminal).
      */
     void ( *prkcolor ) ()  = nullptr;
     /*
     * Opcje wyboru menu.
     */
     std::vector<std::string> options;
     /*
     * Zmienna rozmiaru opcji w menu.
     */
     short size = 0;

     /**
     * Metoda obsługująca wybór opcji z menu.
     * Method choice menu options.
     *
     * */
     int enter ();

protected:
     /*
      * Zmienna sterująca przebiegiem głównej pętli menu.
      */
     bool run = true;
     /*
      * Zmienna sterująca błędami.
      */
     bool error = false;

     /**
      * Metoda wypisuje menu wyboru.
      */
     int menu_choice();
	 
     /**
      * Metoda przerywa pętle w metodzie @see void main_loop(),
      * zmieniając zmienna run z true na false.
      */
     void end ();
	 
	 /**
	  * Metoda zatrzymuje program wyświetlając < enter: > 
	  */
	 void stop_enter() const;
	 
	 /**
	  * Metoda zwraca pointer do funkcji koloru ustawionego 
	  * w menu.
	  */
	 fprkcolor get_menu_color() const;

}; // end class
}; // end namespace
#endif // _TMENU_H_
