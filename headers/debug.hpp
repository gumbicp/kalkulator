#ifndef __DEBUG_H__
#define __DEBUG_H__

#include <sstream>
#include <string>
#include <stdio.h>
namespace rk {

/**
 * Metoda zwraca aktualny lokalny czas.
 */
inline std::string current_time();
// licznik kroków
static long long step = 0;
// poziomy logowania
enum LogLevel {ERROR, WARNING, INFO, DEBUG};

/**
 * Klasa szablonowa logu.
 */
template <typename T>
class Log {
public:
     /**
     * C-tor domyślny
     */
     Log() {}

     /**
     * D-tor
	 * dodaje na end strumienia endl i wysyła os.c_str() 
	 * do metody klasy LogFile output
     */
     virtual ~Log();

     /**
     * Sformatowane wyście .
	 * @return os
     */
     std::ostringstream& get ( LogLevel level = INFO );

     /**
     * Funkcja zwraca zmienną LogLevel ustawioną
	 * standardowo na DEBUG.
	 * @return static LogLevel value
     */
     static LogLevel& _report_level();

     /**
     * Funkcja zwraca poziom debugowania w stringu.
	 * @return str
     */
     static std::string _to_string ( LogLevel level );

     /**
     * Funkcja zwraca poziom debugowania typu LogLevel.
	 * @return LogLevel
     */
     static LogLevel _from_string ( const std::string& level );
protected:
     /**
     * Zmienna dla naszego strumienia.
     */
     std::ostringstream os;

private:
     /**
     * C-tor kopiujący
     */
     Log ( const Log& );

     /**
     * Kopiowanie przez operator  = 
     */
     Log& operator = ( const Log& );
};

template <typename T>
std::ostringstream& Log<T>::get ( LogLevel level )
{
     os << step++ << ") ";
     os << "- " << current_time();
     os << " " << _to_string ( level ) << ": ";
     os << std::string ( level > DEBUG ? level - DEBUG : 0, '\t' );
     return os;
}

template <typename T>
Log<T>::~Log()
{
     os << std::endl;
     T::output ( os.str() );
}

template <typename T>
LogLevel& Log<T>::_report_level()
{
     static LogLevel reportingLevel = DEBUG;
     return reportingLevel;
}

template <typename T>
std::string Log<T>::_to_string ( LogLevel level )
{
     static const char* const buffer[] = {"ERROR", "WARNING", "INFO", "DEBUG"};
     return buffer[level];
}

template <typename T>
LogLevel Log<T>::_from_string ( const std::string& level )
{
     if ( level == "DEBUG" ) {
          return DEBUG;
     }
     if ( level == "INFO" ) {
          return INFO;
     }
     if ( level == "WARNING" ) {
          return WARNING;
     }
     if ( level == "ERROR" ) {
          return ERROR;
     }
     Log<T>().get ( WARNING ) << "Unknown logging level '" << level << "'. Using INFO level as default.";
     return INFO;
}

class LogFile {
public:
     static FILE*& Stream();
     static void output ( const std::string& msg );
};

inline FILE*& LogFile::Stream()
{
     static FILE* pStream = stderr;
     return pStream;
}

inline void LogFile::output ( const std::string& msg )
{
     FILE* pStream = Stream();
     if ( !pStream ) {
          return;
     }
     fprintf ( pStream, "%s", msg.c_str() );
     fflush ( pStream );
}
}; // namespace
#define FILELOG_DECLSPEC
namespace rk {
class FILELOG_DECLSPEC FILELog : public Log<LogFile> {};
//typedef Log<LogFile> FILELog;
}; // namespace
#ifndef FILELOG_MAX_LEVEL
#define FILELOG_MAX_LEVEL rk::DEBUG
#endif

#define FILE_LOG(level) \
    if (level > FILELOG_MAX_LEVEL) ;\
    else if (level > rk::FILELog::_report_level() || !rk::LogFile::Stream()) ; \
    else rk::FILELog().get(level)

#include <sys/time.h>
namespace rk {

inline std::string current_time()
{
     char buffer[11];
     time_t t;
     time ( &t );
     tm r = {0};
     strftime ( buffer, sizeof ( buffer ), "%X", localtime_r ( &t, &r ) );
     struct timeval tv;
     gettimeofday ( &tv, 0 );
     char result[100] = {0};
     sprintf ( result, "%s.%03ld", buffer, ( long ) tv.tv_usec / 1000 );
     return result;
}
}; // namespace
#endif //__DEBUG_H__
