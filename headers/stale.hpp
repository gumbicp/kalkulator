#ifndef STALE_H
#define STALE_H

#include <map>
#include <string>
#include <iterator>

namespace rk {

/*
 * Przykładowe spotykane wartości rezystywności gruntów:

	woda morska 0,5 Ωm
	grunt bagnisty 30–50 Ωm
	ziemia orna 90–150 Ωm
	grunt gliniasty 20–200 Ωm
	wilgotny żwir 100–500 Ωm
	suchy piasek 500 Ωm
	grunt piaszczysty suchy 500–2000 Ωm
	grunt kamienisty 100–3000 Ωm

 * */
/// Stale
///
///		Zbierałem je z różnych publikacji. Zależnie od źródła są w nich pewne różnice.
/// Te tutaj to zazwyczaj te, które były po środku rozrzutu tych wartości.
const double PI = 3.14159265359;
//------------------------------------------------------------------------
const double Cu_R = 0.0175; 			// miedź - [Ω * mm² / m] Opory właściwe w temp. 20 °C
const double Cu_G = 57.0; 				// miedź - [m/Ω mm²] Przewodność właściwa temp. 20 °C
const double Cu_t = 0.00393; 			// miedź - [1/°C] Współczynnik temperaturowy oporu.
//------------------------------------------------------------------------
const double Ag_R = 0.0159;				// srebro - [Ω * mm² / m] Opory właściwe w temp. 20 °C
const double Ag_G = 62.5;				// srebro - [m/Ω mm²] Przewodność ( γ ) właściwa temp. 20 °C
const double Ag_t = 0.0041;				// srebro - [1/°C] Współczynnik temperaturowy oporu.
//------------------------------------------------------------------------
const double Au_R = 0.0244;				// złoto - [Ω * mm² / m] Opory właściwe w temp. 20 °C
const double Au_G = 41;					// złoto - - [m/Ω mm²] Przewodność ( γ ) właściwa temp. 20 °C
//const double Au_t =
//------------------------------------------------------------------------
const double Al_R = 0.0283;				// aluminium - [Ω * mm² / m] Opory właściwe w temp. 20 °C
const double Al_G = 35.3;				// aluminium - [m/Ω mm²] Przewodność właściwa temp. 20 °C
const double Al_t = 0.0039;				// aluminium - [1/°C] Współczynnik temperaturowy oporu.
//------------------------------------------------------------------------
const double W_R = 0.0560;				// wolfram - [Ω * mm² / m] Opory właściwe w temp. 20 °C
//------------------------------------------------------------------------
const double Pt_R = 0.111;				// platyna - [Ω * mm² / m] Opory właściwe w temp. 20 °C
//------------------------------------------------------------------------

const std::map< std::string, double > MAT_REZ = {
     {"Cu",Cu_R},
     {"Ag",Ag_R},
     {"Au",Au_R},
     {"Al",Al_R},
     {"W",W_R},
     {"Pt",Pt_R}
};
const std::map< std::string, double > MAT_G = {
     {"Cu",Cu_G},
     {"Ag",Ag_G},
     {"Au",Au_G},
     {"Al",Al_G}
};
const std::map< std::string, double > MAT_T = {
     {"Cu",Cu_t},
     {"Ag", Ag_t}

};
const short DLUGOSC_LICZBY_WYJSCIE = 6;
};
#endif /* STALE_H */
