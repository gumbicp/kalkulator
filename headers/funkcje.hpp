#ifndef FUNKCJE_HPP
#define FUNKCJE_HPP

#include <cstdlib> 					//std::exit(int status)
#include <iostream>
#include <iomanip>
#include <cstdlib>

#include "kolor.hpp"
#include "debug.hpp"

namespace rk {
/**
 * Funkcja zmienia flagę z True na False lub False na True.
 * */
void zmien_flage ( bool &flaga );
/**
 * Funkcja wypisuje Manual na ekran
 * */
void opis();
/**
 * Funkcja wypisuje błędy
 * */
void blad ( std::string );
/**
 * Funkcja wypisuje wiadomość na ekran.
 * @param string: tekst wiadomości
 * @param bool: argument opcjonalny, gdy zmienimy na
 * 				False, wypisuje tekst bez końca linii.
 *  * */
void cout ( const std::string, bool endl=true );
void clear_terminal();
void error_msg ( std::string );
};
#endif /* FUNKCJE_HPP */
