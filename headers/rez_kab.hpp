/*
 * rez_kab.hpp
 *
 * Copyright 2015 gumbicp <pracacp@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Obliczanie rezystancji kabla;
 *
 *
 */
#ifndef REZ_KAB_HPP
#define REZ_KAB_HPP

#include <iostream>
#include <string>
#include <boost/lexical_cast.hpp>	// rzutowanie liczb na stringi itp
#include "debug.hpp"
#include "stale.hpp"

namespace rk {
using boost::lexical_cast;
using boost::bad_lexical_cast;

class Kabel {
     /**
      * Klasa do przeliczania wartości rezystancji
      * kabli elektrycznych.
      *
      * @author gumbicp
      * @version 0.0.1
      * @date 2015.02.08
      *
      */
protected:
     double L = 0; 	//[m]
     double d = 0; 	//[mm]
     double S = 0;	//[mm^2]
     double ro = 0;	//materiał
     double R = 0;   //Ω
     double gamma1 = 0;  // γ konduktywność
	 double gamma2 = 0;
     /**
      * Prywatna funkcja rzutowania wartości double na string.
      * @returns str from double
      */
     std::string cast_double_to_string ( double );

     /**
     * Funkcja liczy przekrój kabla wzorem:
     * S = PI * d^2 / 4
     *
     * @return double value
     * */
     double wzor_przekroj_kabla();
     /**
     * Funkcja liczy rezystancje kabla wzorem:
     * R = ro * (L/S)
     * @return  double value
     * */
     double wzor_rezystancja_kabla();
	 /**
	  * Funkcja liczy przekrój kabla wzorem:
	  * S = (ro * L) / R
	  * @return double value
	  */
	 double wzor_jaki_przekroj();


public:
     /* Konstruktor domyślny */
     Kabel() {}

     /* Konstruktor */
     Kabel ( double L, double d, double S, double ro, double R , double gamma1, double gamma2);

     /* Destruktor */
     virtual ~Kabel() {}

     void set_L ( double dlugosc ) 	{
          this->L = dlugosc;
     }
     void set_d ( double srednica );
     void set_S ( double przekroj ) {
          this->S = przekroj;
     }
     void set_ro ( double material_opor_wlasciwy ) {
          this->ro = material_opor_wlasciwy;
     }
     void set_R ( double rezystancja );
	 void set_G1 ( double gamma1) {
		  this->gamma1 = gamma1;
	 }
	 void set_G2 ( double gamma2) {
		  this->gamma2 = gamma2;
	 }

     // gettery
     double get_L() 	{
          return this->L;
     }
     double get_d()  {
          return this->d;
     }
     double get_S()  {
          return this->S;
     }
     double get_ro() {
          return this->ro;
     }
     double get_R()  {
          return this->R;
     }
     double get_G1() {
		  return this->gamma1;
     }
     double get_G2() {
		  return this->gamma2;
     }

     std::string get_L_str() {
          return cast_double_to_string ( this->L );
     }
     std::string get_d_str() {
          return cast_double_to_string ( this->d );
     }
     std::string get_S_str() {
          return cast_double_to_string ( this->S );
     }
     std::string get_ro_str() {
          return cast_double_to_string ( this->ro );
     }
     std::string get_R_str() {
          return cast_double_to_string ( this->R );
     }
     std::string get_G1_str() {
          return cast_double_to_string ( this->gamma1 );
     }
     std::string get_G2_str() {
          return cast_double_to_string ( this->gamma2 );
     }

     /// Funkcje klasy

     /**
      * Funkcja liczy przekrój kabla przy znanej oporności, długości i 
	  * materiale.
      * @return str value S
      */
     std::string licz_przekroj_kabla_zadana_opornosc();

     /**
      * Funkcja liczy
      * @return str value R
      */
     std::string licz_rezystancje_kabla();

     /**
     * Funkcja liczy długość kabla.
     * Przy założeniach R, ro, S (d) -> L ?
     * @return str value L
     */
     std::string licz_dlugosc_kabla_dana_rezystancja();

     /**
      * Funkcja przelicza przekrój kabla przy zmianie jego materiału
	  * na inny np. wymiana kabla miedzianego na aluminiowy.
	  * Zakładamy że mają mieć tą samą długość zmienia się tylko materiał.
      * @return str value S
      */
     std::string zmiana_materialu();

     /**
      * Funkcja liczy zmiany rezystancji zależnie od temperatury.
      * @return str value R
      */
     std::string zmiana_rezystancji_od_temperatury();
};


} // namespace rk
#endif /*REZ_KAB_HPP */
