/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2015  gumbicp <email>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */


#ifndef _WIREMENU_H_
#define _WIREMENU_H_

#include "tmenu.hpp"
#include "rez_kab.hpp"

namespace rk {

/// Opcje dla klasy WireMenu
const std::vector<std::string> WIREMENU_OPTIONS = {
     "\tKalkulator rezystancji przewodów.\n",
     "Opcje:\n",
     "\t1) Licz Opór Kabla.\n",
     "\t2) Zmień Materiał Kabla\n",
	 "\t3) Licz Przekrój Kabla\n",
     "\t5) Dostępne materiały\n",
     "\t0) Wyjście z Menu.\n"
};
/// Rozmiar (ilość) opcji w klasie + 1.
const short WIREMENU_SIZE = 6;

/**
 * Klasa WireMenu rozszerza publicznie klasę Menu.
 * Przeznaczona do obliczania rezystancji przewodów.
 *
 * @version 0.1
 * @author gumbicp
 * @email pracacp@gmail.com
 *
 */
class WireMenu : public Menu {
private:
     /// zmienne dla kabla
     bool f_S = false;
     bool f_lenght = false;
     bool f_material = false;
	 bool f_R = false;
	 bool f_G1 = false;
	 bool f_G2 = false;

     ///obiekt klasy Kabel.
     Kabel kobj = Kabel();

     /// Prywatne Metody klasy WireMenu
     void set_wire_value_for_r_count();
	 void set_wire_value_for_s_count();
	 void change_wire_material();
     void enter_values ( std::string value );
     void reset_flags_kobj();
	 
	 

     /**
      * Metoda wypisuje dostępne materiały i ich rezystancje.
      */
     void materials_resitance_cout() const;
     /**
      * Metoda ustawia materiał z przysłanej odpowiedzi (rezystancje)
      * @param answ materiał
      */
     bool set_materials_r ( std::string answ );
	 /**
      * Metoda ustawia materiał z przysłanej odpowiedzi (konduktywność)
      * @param answ materiał
      */
     bool set_materials_g ( std::string answ, std::string value );

public:
     /**
      * Konstruktor
      * - ustawia kolor tekstu menu w terminalu.
      * - do klasy Menu wysyła rozmiar (WIREMENU_SIZE) i opcje dla menu (WIREMENU_OPTIONS)
      */
     WireMenu();
     /**
      * Destruktor
      */
     virtual~WireMenu();

     /**
      * Metoda głównej pętli menu klasy WireMenu.
      */
     void main_loop();

};// class
}; // namespace
#endif /* _WIREMENU_H_ */
