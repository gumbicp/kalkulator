
#ifndef COLOR_TEXT_TERMINAL_H
#define COLOR_TEXT_TERMINAL_H

#include <cstdio>		// Dla printf

namespace rk 
{
	/**
	 * Kasuje wszystkie wcześniejsze ustawienia do trybu domyślnego (czarno biały terminal).
	 * -- [0m
	 * */
	void reset(); 
	/**
	 * Włącza pogrubienie.
	 * -- [1m
	 * */
	void bold();
	/**
	 *  Włącza kursywa.
	 * -- [3m
	 * */
	void italics();
	/**
	 *  Włącza podkreślenie.
	 * -- [4m  
	 * */
	void underline();
	/**
	 * Odwraca ustawienia kolorów, zamienia kolor tła z kolorem tekstu i na odwrót.
	 * -- [7m
	 * */
	void inverse();
	/**
	 * Włącza przekreślenie.
	 * -- [9m    
	 * */
	void strikethrough();
	/**
	 * Wyłącza pogrubienie.
	 * -- [22m 
	 * */
	void bold_off(); 
	/**
	 * Wyłącza kursywę.
	 * -- [23m 
	 * */
	void italics_off(); 
	/**
	 * Wyłącza podkreślenie.
	 * -- [24m
	 * */
	void underline_off(); 
	/**
	 * Wyłącza tryb odwrócenia ustawień.
	 * -- [27m
	 * */
	void inverse_off(); 
	/**
	 * Wyłącza przekreślenie.
	 * -- [29m
	 * */
	void strikethrough_off(); 
	/**
	 * Miganie
	 * -- [5m
	 * */
	void blink();
	/**
	 * Miganie
	 * -- [25m
	 * */
	void blink_off();
	/**
	 * Kolor tekstu - czarny.
	 * -- [30m
	 * */
	void fg_black(); 
	/**
	 * Kolor tekstu - czerwony.
	 * -- [31m
	 * */
	void fg_red(); 
	/**
	 * Kolor tekstu - zielony.
	 * -- [32m
	 * */
	void fg_green(); 
	/**
	 * Kolor tekstu - żółty.
	 * -- [33m
	 * */
	void fg_yellow(); 
	/**
	 * Kolor tekstu - niebieski.
	 * -- [34m
	 * */
	void fg_blue(); 
	/**
	 * Kolor tekstu - magneta.
	 * -- [35m
	 * */
	void fg_magneta(); 
	/**
	 * Kolor tekstu - cyan.
	 * -- [36m
	 * */
	void fg_cyan(); 
	/**
	 * Kolor tekstu - biały.
	 * -- [37m
	 * */
	void fg_white(); 
	/**
	 * Domyślny kolor tekstu (najczęściej biały).
	 * -- [39m
	 * */
	void fg_default(); 
	/**
	 * Kolor tła - czarny.
	 * -- [40m
	 * */
	void bg_black(); 
	/**
	 * Kolor tła - czerwony.
	 * -- [41m
	 * */
	void bg_red(); 
	/**
	 * Kolor tła - zielony.
	 * -- [42m
	 * */
	void bg_green(); 
	/**
	 * Kolor tła - żółty.
	 * -- [43m
	 * */
	void bg_yellow(); 
	/**
	 * Kolor tła - niebieski
	 * -- [44m
	 * */
	void bg_blue(); 
	/**
	 * Kolor tła - magneta
	 * -- [45m
	 * */
	void bg_magneta();
	/**
	 * Kolor tła - cyan
	 * -- [46m  
	 * */
	void bg_cyan(); 
	/**
	 *  Kolor tła - biały
	 * -- [47m
	 * */
	void bg_white();
	/**
	 * Domyślny kolor tła (najczęściej czarny)
	 * -- [49m
	 * */
	void bg_default(); 
	
} // end namespace
#endif /* KOLOR_TEXT_TERMINAL_H */
