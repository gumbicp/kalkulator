OBJECT = *.o
FLAGS = -Wall -std=c++11
F_OUT = bin/kalkulator
MAIN = src/*.cpp 
HEADER = headers/*.hpp

	
default: compile
	@echo 
	@echo Linkowanie $(OBJECT) do pliku wynikowego $(F_OUT)
	@echo
	g++ $(FLAGS) -o  $(F_OUT) $(OBJECT)
	
compile: $(HEADER)
	@echo
	@echo Kompilacja $(MAIN)
	@echo
	g++ $(FLAGS) -c  $(MAIN)
	
clean.f: 
	@echo Usuwanie pliku wynikowego
	rm $(F_OUT)
	
clean: clean.f
	@echo Usuwanie plików *.o
	rm *.o