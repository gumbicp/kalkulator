/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2015  gumbicp <email>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */
#include "../headers/wiremenu.hpp"

rk::WireMenu::WireMenu() : Menu ( WIREMENU_OPTIONS, WIREMENU_SIZE )
{
     FILE_LOG ( INFO ) << "\nC-or MainMenu::MainMenu(char ** argv) ";
     rk::Menu::set_menu_color ( & rk::fg_cyan );
}
rk::WireMenu::~WireMenu()
{
     FILE_LOG ( INFO ) << "\nWireMenu::~WireMenu()";

}
void rk::WireMenu::main_loop()
{
     int odp = 0;

     while ( rk::Menu::run ) {

          FILE_LOG ( INFO ) << "\nWireMenu::main_loop() w pętli while(RUN)";

          // gdy zmienne są już ustawione wypisanie wyników i resetowanie zmiennych
          if ( f_lenght && f_material && f_S ) {
               rk::Menu::get_menu_color() ();
               rk::cout ( "Kabel o długości " + kobj.get_L_str() +
                          " m, przekroju " + kobj.get_S_str() +
                          "mm² i ro = " + kobj.get_ro_str() +
                          " [Ω * mm² / m]" );

               rk::cout ( "ma oporność: " + kobj.get_R_str() +" Ω.  " );
               rk::Menu::stop_enter();
               odp = -1;
               reset_flags_kobj();
          } else if ( f_lenght && f_material && f_R ) {
               rk::Menu::get_menu_color() ();
               rk::cout ( "Kabel o długości " + kobj.get_L_str() +
                          " m, oporności " + kobj.get_R_str() +
                          "Ω i ro = " + kobj.get_ro_str() +
                          " [Ω * mm² / m]" );

               rk::cout ( "powinien mieć przekrój: " + kobj.get_S_str() +" mm².  " );
               rk::Menu::stop_enter();
               odp = -1;
               reset_flags_kobj();
          } else if ( f_S && f_G1 && f_G2 ) {
               rk::Menu::get_menu_color() ();
               rk::cout ( "Po zamianie materiału kabel powinien mieć przekrój: " + kobj.get_S_str() +" mm².  " );
               rk::Menu::stop_enter();
               odp = -1;
               reset_flags_kobj();
          } else {
               //wracamy do wyboru opcji menu i ustawiamy zmienna odp dla warunków poniżej
               odp = rk::Menu::menu_choice();
               FILE_LOG ( DEBUG ) << "WireMenu::main_loop() w pętli while(RUN) - odp = " << odp;
          }

          // koniec programu
          if ( odp == 0 ) {
               end ();
          }
          // dla pierwszej opcji z menu
          else if ( odp == 1 ) {
               set_wire_value_for_r_count();
               //reset_flags_kobj();
          }
          //dla drugiej opcji
          else if ( odp == 2 ) {
               change_wire_material();
          }
          // dla trzeciej opcji
          else if ( odp == 3 ) {
               set_wire_value_for_s_count();
               // reset_flags_kobj();
          }
          // dla piątej opcji menu
          else if ( odp == 5 ) {
               materials_resitance_cout();
          }
     }

     FILE_LOG ( INFO ) << "WireMenu::main_loop() ** end MainMenuKabla **";

}
void rk::WireMenu::reset_flags_kobj()
{
     FILE_LOG ( INFO ) << "\nMainMenuKabla::reset_flags_kobj()";
     this->f_material = false;
     this->f_lenght = false;
     this->f_S = false;
     this->f_R = false;
     this->f_G1 = false;
     this->f_G2 = false;

     kobj.set_d ( 0 );
     kobj.set_S ( 0 );
     kobj.set_L ( 0 );
     kobj.set_R ( 0 );
     kobj.set_ro ( 0 );
     kobj.set_G1 ( 0 );
     kobj.set_G2 ( 0 );
}
void rk::WireMenu::set_wire_value_for_r_count()
{
     FILE_LOG ( INFO ) << "\nMainMenuKabla::set_wire_value_for_r_count()";
     rk::clear_terminal();
     rk::Menu::get_menu_color() ();
     rk::cout ( "Jeżeli chcesz podać przekrój zamiast średnicy wpisz p" );
     rk::cout ( "Podaj długość kabla (metry):" );
     enter_values ( "L" );
     rk::cout ( "Podaj średnicę (milimetry):" );
     enter_values ( "d" );
     rk::cout ( "Podaj przekrój (milimetry kwadrat):" );
     enter_values ( "S" );
     rk::cout ( "Podaj materiał (kod programu):" );
     enter_values ( "x" );
     kobj.licz_rezystancje_kabla();
}
void rk::WireMenu::set_wire_value_for_s_count()
{
     FILE_LOG ( INFO ) << "\nMainMenuKabla::set_wire_value_for_s_count()";
     rk::clear_terminal();
     rk::Menu::get_menu_color() ();
     rk::cout ( "Podaj długość kabla (metry):" );
     enter_values ( "L" );
     rk::cout ( "Podaj opór kabla:" );
     enter_values ( "R" );
     rk::cout ( "Podaj materiał (kod programu):" );
     enter_values ( "x" );
     kobj.licz_przekroj_kabla_zadana_opornosc();
}
void rk::WireMenu::change_wire_material()
{
     FILE_LOG ( INFO ) << "\nWireMenu::change_wire_material()";
     rk::clear_terminal();
     rk::Menu::get_menu_color() ();
     rk::cout ( "Podaj przekrój (milimetry kwadrat)" );
     enter_values ( "S" );
     rk::cout ( "Podaj materiał kabla zmienianego(np Cu):" );
     enter_values ( "G1" );
     rk::cout ( "Podaj materiał zamiennika (np Al):" );
     enter_values ( "G2" );
     kobj.zmiana_materialu();
}
void rk::WireMenu::enter_values ( std::string value )
{
     FILE_LOG ( INFO ) << "\nMainMenuKabla::enter_values(std::string value)";
     FILE_LOG ( DEBUG ) << "value = " << value;

     using std::cin;
     //zmienne tymczasowe
     double odp = 0;
     std::string txt_odp;

     while ( true ) {

          if ( error ) {
               if ( value == "x" || value == "G1" || value == "G2")
                    error_msg ( "Podaj Materiał np Cu dla miedzi !!" );
               else
                    error_msg ( "Podaj Liczbę , zera też nie oblecą !!" );
               rk::Menu::get_menu_color() ();
               error = false;
          }
          try {
               // pobieranie odpowiedzi
               rk::cout ( "<enter> :", false );
               cin >> txt_odp;
               /// logowanie
               FILE_LOG ( DEBUG ) << "txt_odp = " << txt_odp;
               // Sprawdzamy czy wybieramy materiał "x" , czy są ustawione inne zmienne
               // i wysyłamy do metody set_materials_r odpowiedź .
               // Jeżeli jest w stałych to ustawi wartość wybranego materiału i
               // zwróci true.
               if ( value == "x" && f_lenght && ( f_S || f_R ) && set_materials_r ( txt_odp ) ) {
                    f_material = true;
                    break;
               } else if ( value == "G1" && f_S && set_materials_g ( txt_odp, value ) ) {
                    this->f_G1 = true;
                    break;
               } else if ( value == "G2" && f_S && f_G1 && set_materials_g ( txt_odp, value ) ) {
                    this->f_G2 = true;
                    break;
               }
               // warunek pomijania wypełniania dla zmiennych średnica i przekrój
               if ( txt_odp == "p" ) {
                    FILE_LOG ( DEBUG ) << " Pomijamy ";
                    break;
               }
               // rzutowanie na odpowiedzi na liczbę , nie liczba wyrzuci wyjątek
               odp = boost::lexical_cast<double> ( txt_odp );

               FILE_LOG ( DEBUG ) << " odp  = " << odp << "------- do ustawienia";

               // zabezpieczenie przed zerowymi wartościami zmiennych
               if ( odp <= 0 ) {
                    error = true;
               }
               // brak błędu, wyskakujemy za pętle i ustawiamy zmienne
               if ( !error ) break;

          } catch ( boost::bad_lexical_cast & ) {
               error = true;
          }
     } // koniec pętli while

     if ( value == "L" && !error ) {
          kobj.set_L ( odp );
          f_lenght = true;
          FILE_LOG ( DEBUG ) << " w if długości -- odp = " << odp << ", kobj.get_L() = " << kobj.get_L();
     } else if ( value == "d" && txt_odp != "p" && !error ) {
          kobj.set_d ( odp );
          f_S = true;
          FILE_LOG ( DEBUG ) << " w if średnicy-- odp = " << odp << ", kobj.get_d() = " << kobj.get_d();
     } else if ( value == "S" && !f_S && !error ) {
          kobj.set_S ( odp );
          f_S = true;
          FILE_LOG ( DEBUG ) << " w if przekrój-- odp = " << odp<< ", kobj.get_S() = " << kobj.get_S();
     } else if ( value == "R" && !error ) {
          kobj.set_R ( odp );
          f_R = true;
     } else if ( value == "x" && !f_material && !error ) {
          FILE_LOG ( WARNING ) << "Nie Ustawiony materiał !!!";
          enter_values ( "x" );
     } else if ( ( value == "G1" || value == "G2" ) && !error ) {
          FILE_LOG ( WARNING ) << "Nie Ustawiony materiał !!!";
          if ( !f_G1 )
               enter_values ( "G1" );
          else if ( value == "G2" && !f_G2 )
               enter_values ( "G2" );
     }


}
void rk::WireMenu::materials_resitance_cout() const
{
     FILE_LOG ( INFO ) << "\nMainMenuKabla::materials_resitance_cout() const";

     rk::Menu::get_menu_color() ();

     std::cout << "Dostępne materiały [Ω * mm² / m] Opory właściwe w temp. 20 °C " << std::endl << std::endl;
     std::cout << "Nazwa - kod w programie =\trezystancja" << std::endl;
     for ( auto it = rk::MAT_REZ.begin(); it != rk::MAT_REZ.end(); ++it ) {
          std::cout << "\t" << it->first << " =\t" << it->second << '\n';
     }
     std::cout << std::endl;
	 std::cout << "Dostępne materiały [m/Ω mm²] Przewodność ( γ ) właściwa temp. 20 °C" << std::endl;
	 std::cout << "Nazwa - kod w programie =\tkonduktywność" << std::endl;
	 for ( auto itr = rk::MAT_G.begin(); itr != rk::MAT_G.end(); ++itr ) {
          std::cout << "\t" << itr->first << " =\t" << itr->second << '\n';
     }
     rk::Menu::stop_enter();
     rk::reset();
}

bool rk::WireMenu::set_materials_r ( std::string answ )
{
     FILE_LOG ( INFO ) << "\nWireMenu::set_materials_r ( std::string answ )";

     for ( auto it = rk::MAT_REZ.begin(); it != rk::MAT_REZ.end() ; ++it ) {
          if ( boost::lexical_cast<std::string> ( it->first ) == answ ) {
               FILE_LOG ( DEBUG ) << "Ustawiony materiał na "<< it->first << " - "<< it->second;
               kobj.set_ro ( it->second );
               return true;
          }
     }
     FILE_LOG ( DEBUG ) << "BRAK USTAWIONEGO MATERIALU !!! odp = " << answ;
     return false;
}
bool rk::WireMenu::set_materials_g ( std::string answ, std::string value )
{
     FILE_LOG ( INFO ) << "\nWireMenu::set_materials_g ( std::string answ )";

     for ( auto it = rk::MAT_G.begin(); it != rk::MAT_G.end() ; ++it ) {
          if ( boost::lexical_cast<std::string> ( it->first ) == answ ) {
               FILE_LOG ( DEBUG ) << "Ustawiony materiał na "<< it->first << " - "<< it->second;
               if ( value == "G1" ) {
                    kobj.set_G1 ( it->second );
               } else if ( value == "G2" ) {
                    kobj.set_G2 ( it->second );
               }
               return true;
          }
     }
     FILE_LOG ( DEBUG ) << "BRAK USTAWIONEGO MATERIALU !!! odp = " << answ;
     return false;
}


