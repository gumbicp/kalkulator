#include "../headers/kolor.hpp"
/**
 * Kasuje wszystkie wczesniejsze ustawienia do trybu domyślnego (czarno biały terminal).
 * -- [0m
 * */
void rk::reset()
{
	printf("\x1b[0m\n");
}
/**
 * Włącza pogrubienie.
 * -- [1m
 * */
void rk::bold()
{
	printf("\x1b[1m\n");
}
/**
 *  Włącza kursywa.
 * -- [3m
 * */
void rk::italics()
{
	printf("\x1b[3m\n");
}
/**
 *  Włącza podkreślenie.
 * -- [4m  
 * */
void rk::underline()
{
	printf("\x1b[4m\n");
}
/**
 * Odwraca ustawienia kolorów, zamienia kolor tła z kolorem tekstu i na odwrót.
 * -- [7m
 * */
void rk::inverse()
{
	printf("\x1b[7m\n");
}
/**
 * Włącza przekreślenie.
 * -- [9m    
 * */
void rk::strikethrough()
{
	printf("\x1b[9m\n");
}
/**
 * Miganie
 * -- [5m
 * */
void rk::blink()
{
	printf("\x1b[5m\n");
}
/**
 * Miganie
 * -- [25m
 * */
void rk::blink_off()
{
	printf("\x1b[25m\n");
}
/**
 * Wyłącza pogrubienie.
 * -- [22m 
 * */
void rk::bold_off()
{
	printf("\x1b[22m\n");
}
/**
 * Wyłącza kursywe.
 * -- [23m 
 * */
void rk::italics_off()
{
	printf("\x1b[23m\n");
}
/**
 * Wyłącza podkreślenie.
 * -- [24m
 * */
void rk::underline_off()
{
	printf("\x1b[24m\n");
}
/**
 * Wyłącza tryb odwrócenia ustawień.
 * -- [27m
 * */
void rk::inverse_off()
{
	printf("\x1b[27m\n");
}
/**
 * Wyłącza przekreślenie.
 * -- [29m
 * */
void rk::strikethrough_off()
{
	printf("\x1b[29m\n");
}
/**
 * Kolor tekstu - czarny.
 * -- [30m
 * */
void rk::fg_black()
{
	printf("\x1b[30m\n");
}
/**
 * Kolor tekstu - czerwony.
 * -- [31m
 * */
void rk::fg_red()
{
	printf("\x1b[31m\n");
}
/**
 * Kolor tekstu - zielony.
 * -- [32m
 * */
void rk::fg_green()
{
	printf("\x1b[32m\n");
}
/**
 * Kolor tekstu - żółty.
 * -- [33m
 * */
void rk::fg_yellow()
{
	printf("\x1b[33m\n");
}
/**
 * Kolor tekstu - niebieski.
 * -- [34m
 * */
void rk::fg_blue()
{
	printf("\x1b[34m\n");
}
/**
 * Kolor tekstu - magneta.
 * -- [35m
 * */
void rk::fg_magneta()
{
	printf("\x1b[35m\n");
}
/**
 * Kolor tekstu - cyan.
 * -- [36m
 * */
void rk::fg_cyan()
{
	printf("\x1b[36m\n");
}
/**
 * Kolor tekstu - biały.
 * -- [37m
 * */
void rk::fg_white()
{
	printf("\x1b[37m\n");
}
/**
 * Domyślny kolor tekstu (najczęściej biały).
 * -- [39m
 * */
void rk::fg_default()
{
	printf("\x1b[39m\n");
}
/**
 * Kolor tła - czarny.
 * -- [40m
 * */
void rk::bg_black()
{
	printf("\x1b[40m\n");
}
/**
 * Kolor tła - czerwony.
 * -- [41m
 * */
void rk::bg_red()
{
	printf("\x1b[41m\n");
}
/**
 * Kolor tła - zielony.
 * -- [42m
 * */
void rk::bg_green()
{
	printf("\x1b[42m\n");
}
/**
 * Kolor tła - żółty.
 * -- [43m
 * */
void rk::bg_yellow()
{
	printf("\x1b[43m\n");
}
/**
 * Kolor tła - niebieski
 * -- [44m
 * */
void rk::bg_blue()
{
	printf("\x1b[44m\n");
}
/**
 * Kolor tła - magneta
 * -- [45m
 * */
void rk::bg_magneta()
{
	printf("\x1b[45m\n");
}
/**
 * Kolor tła - cyan
 * -- [46m  
 * */
void rk::bg_cyan()
{
	printf("\x1b[46m\n");
}
/**
 *  Kolor tła - biały
 * -- [47m
 * */
void rk::bg_white()
{
	printf("\x1b[47m\n");
}
/**
 * Domyślny kolor tła (najczęściej czarny)
 * -- [49m
 * */
void rk::bg_default()
{
	printf("\x1b[49m\n");
}

