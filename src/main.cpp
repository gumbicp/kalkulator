/*
 * rezystancja_kabla.cpp
 *
 * Copyright 2015 gumbicp <gumbicp@linux>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Obliczanie rezystancji kabla;
 *
 * ze wzoru
 */

#include "../headers/bib_rk.hpp"


/*
 * Funkcja main
 * */
int main ( int argc, char **argv )
{
     try {
          rk::FILELog::_report_level() = rk::FILELog::_from_string ( argv[1] ? argv[1] : "DEBUG" );
          FILE *lFile = fopen ( "program.log", "w" );
          rk::LogFile::Stream() = lFile;
		  FILE_LOG ( rk::DEBUG ) << "Tworzymy obiekt MainMenu";
          rk::MainMenu menu  = rk::MainMenu ();
          menu.main_loop();

          return 0;
     } catch ( const std::exception& e ) {
          FILE_LOG ( rk::ERROR ) << e.what();
     }

     return -1;
}

