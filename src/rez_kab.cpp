#include "../headers/rez_kab.hpp"

rk::Kabel::Kabel ( double L, double d, double S, double ro, double R, double gamma1, double gamma2 ) :
     L ( L ), d ( d ), S ( S ), ro ( ro ), R ( R ), gamma1(gamma1), gamma2(gamma2)
{
     FILE_LOG ( INFO ) << "\nKabel::Kabel(double L, double d, double S, double ro, double R, double gamma)";
     FILE_LOG ( DEBUG ) << "this->L = "<< this->L << ", this->d = " << this->d << ", this->S = " << this->S
                        << ", this->ro= " << this->ro << ", this->R = " 
						<< this->R << ", this->gamma1 = " << this->gamma1
						<< ", this->gamma2 = " << this->gamma2;
}


/// private ************************************************

std::string rk::Kabel::cast_double_to_string ( double value )
{
     FILE_LOG ( INFO ) << "\nKabel::cast_double_to_string(double value)";

     std::string tmp = lexical_cast<std::string> ( value );
     tmp.resize ( rk::DLUGOSC_LICZBY_WYJSCIE );
     return tmp;
}

double rk::Kabel::wzor_przekroj_kabla()
{
     FILE_LOG ( INFO ) << "\nKabel::wzor_przekroj_kabla()";
     FILE_LOG ( DEBUG ) << "d = " << this->d;
     return ( ( this->d * this->d ) * PI ) / 4;
}

double rk::Kabel::wzor_rezystancja_kabla()
{
     FILE_LOG ( INFO ) << "\nKabel::wzor_rezystancja_kabla()";
     FILE_LOG ( DEBUG ) << "  S = " << this->S << " L = " << this->L << " ro = " << this->ro;
     if ( this->S != 0 ) {
          return ( this->L/this->S ) * this->ro;
     } else
          FILE_LOG ( ERROR ) << " --------- DZIELENIE PRZEZ S = ZERO ZWRACAMY ZERO !!! ---------";
     return 0;
}
double rk::Kabel::wzor_jaki_przekroj()
{
     FILE_LOG ( INFO ) << "\nKabel::wzor_jaki_przekroj()";
     FILE_LOG ( DEBUG ) << "  R = " << this->R << " L = " << this->L << " ro = " << this->ro;
     if ( this->R != 0 ) {
          return ( this->ro * this->L ) /this->R;
     } else
          FILE_LOG ( ERROR ) << " --------- DZIELENIE PRZEZ R = ZERO ZWRACAMY ZERO !!! ---------";
     return 0;
}



/// public setters ************************************************

void rk::Kabel::set_d ( double srednica )
{
     FILE_LOG ( INFO ) << "\nKabel::set_d(double srednica)";
     this->d = srednica;
     FILE_LOG ( DEBUG ) << "Kabel::set_d(double srednica) przed if Ustawione S na " << get_S();
     if ( this->S == 0 ) {
          set_S ( wzor_przekroj_kabla() );
          FILE_LOG ( DEBUG ) << " rk::Kabel::set_d(double srednica) w if Ustawione S na " << get_S();
     }
}

void rk::Kabel::set_R ( double rezystancja )
{
     FILE_LOG ( INFO ) << "\nKabel::set_R(double rezystancja)";
     this->R = rezystancja;
}


///public functions ************************************************

std::string rk::Kabel::licz_rezystancje_kabla()
{
     FILE_LOG ( INFO ) << "\nKabel::licz_rezystancje_kabla()";
     this->R = wzor_rezystancja_kabla();
     return get_R_str();
}

std::string rk::Kabel::licz_dlugosc_kabla_dana_rezystancja()
{
     FILE_LOG ( INFO ) << "\nKabel::licz_dlugosc_kabla_dana_rezystancja()";
     std::cout << "Not implemented yet..." << std::endl;
     return 0;
}

std::string rk::Kabel::zmiana_materialu()
{
     FILE_LOG ( INFO ) << "\nKabel::zmiana_materialu()";
	 if(this->S != 0){
		this->S = ( this->gamma1 / this->gamma2 ) * this->S;
		return get_S_str();
	 }
	 else{
		 FILE_LOG( ERROR ) << " DZIELENIE PRZEZ ZERO !!! S = " << this->S;
		 return " 0 !!! ";
	 }
}

std::string rk::Kabel::licz_przekroj_kabla_zadana_opornosc()
{
     FILE_LOG ( INFO ) << "\nKabel::licz_przekroj_kabla_zadana_opornosc()";
     this->S = wzor_jaki_przekroj();
     return get_S_str();
}
