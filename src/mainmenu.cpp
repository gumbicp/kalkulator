/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2015  gumbicp <email>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */
#include "../headers/mainmenu.hpp"

rk::MainMenu::MainMenu () : Menu(MAINMENU_OPTIONS, MAINMENU_SIZE)
{
     FILE_LOG ( INFO ) << "C-or rk::MainMenu::MainMenu(char ** argv) ";
	 rk::Menu::set_menu_color(& rk::fg_cyan);
}
rk::MainMenu::~MainMenu()
{
     FILE_LOG ( INFO ) << "rk::MainMenu::~MainMenu()";
}
void rk::MainMenu::main_loop()
{
     int odp = 0;
     while (rk::Menu::run) {

          FILE_LOG ( INFO ) << "void rk::MainMenu::main_loop() w pętli while(RUN)";

          odp = rk::Menu::menu_choice();
          FILE_LOG ( DEBUG ) << "rk::MainMenu::main_loop() w pętli while(RUN) - odp = " << odp;

          if ( odp == 0 ) {
               end ();
          } else if ( odp == 1 ) {
               rk::WireMenu mobj = rk::WireMenu ();
               mobj.main_loop();
          }
     }

     FILE_LOG ( INFO ) << "void rk::MainMenu::start() end programu";

}