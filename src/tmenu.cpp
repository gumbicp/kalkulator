/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2015  gumbicp <email>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "../headers/tmenu.hpp"

rk::Menu::Menu ( std::vector< std::string > opts, short int s ) : size ( s )
{
     FILE_LOG ( INFO ) << "\nMenu::Menu ( std::vector< std::string > opts, short int s )";

     this->options.swap ( opts );
     FILE_LOG ( DEBUG ) << "menu s = " << s;
     FILE_LOG ( DEBUG ) << "menu size = " << size;
}

rk::Menu::~Menu()
{
     FILE_LOG ( INFO ) << "\nMenu::~Menu() options clear() end prkcolor = nullptr";
     options.clear();
     prkcolor = nullptr;
}

int rk::Menu::enter ()
{
     FILE_LOG ( INFO ) << "\nMenu::enter(bool & error, const short & size)";

     using std::cin;
     int answr = -1;
     std::string txt_answr;

     if ( error ) {
          error_msg ( "Nie ma takiej opcji !!" );
          error = false;
     }
     try {
          prkcolor();
          rk::cout ( "<enter> :", false );
          cin >> txt_answr;
          answr = boost::lexical_cast<int> ( txt_answr );
          if ( answr > size || answr < 0 ) {
               error = true;
          }
          rk::reset();
          return answr;

     } catch ( boost::bad_lexical_cast & ) {
          rk::reset();
          error = true;
          return -1;
     }
}

void rk::Menu::set_menu_color ( void ( *pointer ) () )
{
     FILE_LOG ( INFO ) << "\nMenu::set_menu_color ( void ( *pointer ) () )";
     FILE_LOG ( DEBUG ) << "*pointer : " << pointer ;
     prkcolor = pointer;
}

int rk::Menu::menu_choice()
{
     FILE_LOG ( INFO ) << "\nMenu::menu_choice()" << std::endl;

     clear_terminal();
     prkcolor();
     for ( unsigned int i = 0; i < options.size(); i++ ) {
          rk::cout ( options[i] );
     }
     rk::reset();
     return enter ();
}

void rk::Menu::end ()
{
     FILE_LOG ( INFO ) << "\nMenu::end() RUN to false" << std::endl;

     this->run = false;
}
void rk::Menu::stop_enter() const
{
	FILE_LOG ( INFO ) << "\nMenu::stop_enter()" << std::endl;
	std::string stmp;
	rk::cout ( "<enter>: ", false );
	std::cin.ignore();
	std::getline ( std::cin, stmp );
}
rk::fprkcolor rk::Menu::get_menu_color() const
{
	return prkcolor;
}

