#include "../headers/funkcje.hpp"
void rk::zmien_flage ( bool &flaga )
{
     if ( flaga )
          flaga = false;
     else
          flaga = true;
}
void rk::opis()
{
     rk::fg_cyan();
     std::cout <<"NAME" << std::endl;
     std::cout <<"\t Rezystancja_kabla - testowy kalkulator	" << std::endl;
     std::cout <<"SYNOPSIS" << std::endl;
     std::cout << "\t ./rezystancja_kabla [-opcja]  [numer] [-material_przewodu]\n";
     std::cout <<"DESCRIPTION" << std::endl;
     std::cout <<"\t Prosty programik konsolowy do przeliczania oporu w omach kabli z różnych materiałów.\n"
               "\t Podajemy długość, przekrój lub średnice kabla i z jakiego materiału jest zrobiony.\n\t (symbol pierwiastka).\n" << std::endl;
     std::cout <<"OPTIONS" << std::endl;
     std::cout <<"\t[opcja] --> po każdej z nich podajemy wartość liczbową [numer]" << std::endl;
     std::cout <<"\t\t -L długość przewodu (wymagane)" << std::endl;
     std::cout <<"\t\t -S przekrój przewodu (wymagane / lub średnica -d )" << std::endl;
     std::cout <<"\t\t -S średnica przewodu (wymagane / lub przekrój -S )" << std::endl;
     std::cout << "\t[-material_przewodu]\n";
     std::cout << "\t\t -Cu dla kabla z Miedzi\n";
     std::cout << "\t\t -Al dla kabla z Aluminium\n";
     std::cout << "AUTHOR\n";
     std::cout << "\t\t Copyright 2015 gumbicp <pracacp@gmail.com>\n\n"
               "\t This program is free software; you can redistribute it and/or modify\n"
               "\t it under the terms of the GNU General Public License as published by\n"
               "\t the Free Software Foundation; either version 2 of the License, or\n"
               "\t (at your option) any later version.\n\n"
               "\t This program is distributed in the hope that it will be useful,\n"
               "\t but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
               "\t MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
               "\t GNU General Public License for more details."
               "\n\n"
               "\t You should have received a copy of the GNU General Public License\n"
               "\t along with this program; if not, write to the Free Software\n"
               "\t Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,\n"
               "\t MA 02110-1301, USA.\n";
     rk::reset();
}
void rk::blad ( std::string cout )
{
     FILE_LOG ( WARNING ) << "void rk::blad ( std::string cout ) = " << cout;

     rk::bg_red();
     rk::blink();
     std::cout << cout << std::endl;
     rk::blink_off();
     rk::reset();
     opis();
     std::exit ( 1 );
}
void rk::cout ( const std::string cout, bool endl )
{
     FILE_LOG ( INFO ) << "void rk::cout ( const std::string cout, bool endl )";
     FILE_LOG ( DEBUG ) << "COUT = " << cout << ",\n endl = " << endl ;
     if ( endl )
          std::cout << cout << std::endl;
     else {
          std::cout << cout;
     }
}
void rk::clear_terminal()
{
     FILE_LOG ( INFO ) << "void rk::clear_terminal()" << std::endl;
     system ( "clear" );

}
void rk::error_msg ( std::string msg )
{
     FILE_LOG ( WARNING ) << "rk::error_msg(std::string = "<< msg <<")";
     rk::fg_red();
     rk::cout ( msg );
     rk::reset();
}
